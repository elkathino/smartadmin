<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require __DIR__ . '/../vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

$app->add(new \Slim\Middleware\Session([
    'name' => 'dummy_session',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));
// Get container
$container = $app->getContainer();

$container['session'] = function () {
    return new \SlimSession\Helper;
};
$container['pdo'] = new \PDO('mysql:hostname=localhost;dbname=smartadmin', 'azer', 'azer');

/*$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});*/
// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../templates/', [
        //'cache' => __DIR__ .'/../cache'
    ]);
// Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    return $view;
};
$container[\Smartadmin\Repository\UserRepository::class] = function () use ($container) {
    return new \Smartadmin\Repository\UserRepository($container['pdo']);
};

$authenticate = function (Request $request, Response $response, $next) {
    if (empty($this['session']['user'])) {
        $response->withStatus(302)->withHeader('Location', $this->router->pathFor('login'));
    }
    return $next($request, $response);
};


$anonymously = function (Request $request, Response $response, $next) {
    if (!empty($this['session']['user'])) {
        $response->withStatus(302)->withHeader('Location', $this->router->pathFor('profile'));
    }
    return $next($request, $response);
};

$app->any('/logout', function (Request $request, Response $response) use ($app) {
    unset($this['session']['user']);
    return $response->withStatus(302)->withHeader('Location', $this->router->pathFor('login'));
})->setName('logout');

$app->any('/profile', function (Request $request, Response $response) use ($app) {
    return $this->view->render($response, "user/profile.twig", [
        'user' => $this['session']['user']
    ]);
})->setName('profile')->add($authenticate);

$app->any('/login', function (Request $request, Response $response) use ($app) {
    /*** @var \Smartadmin\Repository\UserRepository $repository */
    $repository = $this[\Smartadmin\Repository\UserRepository::class];
    $errors = [];
    $data = $request->getParsedBody();

    if ($request->getMethod() === 'POST') {
        if (empty($data['login']['email'])) {
            $errors['email'] = 'Ce champ est obligatoire';
        } elseif (!filter_var($data['login']['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Ceci n'est pas un email valide";
        }

        if (empty($data['login']['password'])) {
            $errors['password'] = 'Ce champ est obligatoire';
        }

        if (empty($errors)) {
            $user = $repository->findUserByEmail($data['login']['email']);
            if (false === $user) {
                $errors['email'] = "Ce compte n'existe pas";
            } elseif (!password_verify($data['login']['password'], $user['password'])) {
                $errors['general'] = "L'email ou le mot de passe n'est pas valide";
            } else {
                $this['session']['user'] = $user;
                return $response->withStatus(302)->withHeader('Location', $this->router->pathFor('profile'));
            }
        }
    }

    return $this->view->render($response, "login/login.twig", [
        'errors' => $errors,
        'data' => $data
    ]);
})->setName('login')->add($anonymously);

$app->any('/register', function (Request $request, Response $response) use ($app) {
    /*** @var \Smartadmin\Repository\UserRepository $repository */
    $repository = $this[\Smartadmin\Repository\UserRepository::class];
    $errors = [];
    $data = $request->getParsedBody();

    if ($request->getMethod() === 'POST') {
        if (empty($data['register']['email'])) {
            $errors['email'] = 'Ce champ est obligatoire';
        } elseif (!filter_var($data['register']['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Ceci n'est pas un email valide";
        } else {
            $user = $repository->findUserByEmail($data['register']['email']);
            if (false !== $user) {
                $errors['email'] = "Cet utilisateur existe déjà";
            }
        }

        if (empty($data['register']['firstname'])) {
            $errors['firstname'] = 'Ce champ est obligatoire';
        }

        if (empty($data['register']['lastname'])) {
            $errors['lastname'] = 'Ce champ est obligatoire';
        }

        if (!isset($data['register']['gender'])) {
            $errors['gender'] = 'Ce champ est obligatoire';
        }

        if (empty($data['register']['password']) && empty($data['register']['password_confirmation'])) {
            $errors['password'] = 'Ce champ est obligatoire';
            $errors['password_confirmation'] = 'Ce champ est obligatoire';
        } elseif (empty($data['register']['password'])) {
            $errors['password'] = 'Ce champ est obligatoire';
        } elseif (empty($data['register']['password_confirmation'])) {
            $errors['password_confirmation'] = 'Ce champ est obligatoire';
        } elseif ($data['register']['password'] !== $data['register']['password_confirmation']) {
            $errors['password'] = 'Les deux mots de passes ne sont pas identiques';
        }

        if (empty($errors)) {
            $user = $data['register'];
            $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT, ['cost' => 12]);
            $user['role'] = 'ROLE_USER';
            $repository->add($user);
            $this['session']['flash']['success'] = 'Votre compte a été ajouté avec succès';
            return $response->withStatus(302)->withHeader('Location', $this->router->pathFor('login'));
        }
    }
    return $this->view->render($response, "register/register.twig", [
        'errors' => $errors,
        'data' => $data
    ]);
})->setName('register')->add($anonymously);

$app->run();
