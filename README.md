Notre application vise à assister les immigrés dans leurs démarches administratives

Application : SlimFramework + MySQL + Elasticsearch

télécharger le projet et le mettre dans le dossier www de votre serveur
ou dans n'importe quel dossier si vous configurez un virtual host

Avec git : faire un coup de git clone ..

Installer Composer : https://getcomposer.org/download/
Le mettre dans le path si nécessaire.

Faire : composer update

Importer le fichier smartadmin.sql

test de connexion d'un utilisateur existant :
email : toto@gmail.com
mdp : azerty
