<?php
/**
 * Created by PhpStorm.
 * User: satocorp
 * Date: 29/10/2017
 * Time: 18:21
 */

namespace Smartadmin\Repository;


class UserRepository
{
    /**
     * @var \PDO
     */
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Recuppère un utilisateur en fonction de son email.
     *
     * @param $email
     * @return mixed
     */
    public function findUserByEmail($email)
    {
        $query = $this->pdo->prepare('SELECT * FROM user WHERE email = :email');
        $query->execute(['email' => $email]);
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function add(array $user)
    {
        $query = $this->pdo->prepare('insert into user (email, firstname, lastname, password, gender, role) VALUES (:email, :firstname, :lastname, :password, :gender, :role)');
        $query->execute([
            'email'=>$user['email'],
            'firstname'=>$user['firstname'],
            'lastname'=>$user['lastname'],
            'password'=>$user['password'],
            'gender'=>$user['gender'],
            'role' => $user['role']
        ]);
    }

}